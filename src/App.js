import { Component } from "react";
import styled, { createGlobalStyle } from "styled-components";
import Formulario from "./components/Formulario";
import ListaCitas from "./components/ListaCitas";

const GlobalStyle = createGlobalStyle`
html{
  height: 100%
}
*,*::after,::before{
  margin: 0;
  padding: 0;
  box-sizing: inherit
}
body{
  font-family: Arial, Helvetica, sans-serif;
  height: 100%;
  margin: 0;
  box-sizing: border-box;
  color: #555
}
`;

const Container = styled.div`
  max-width: 1140px;
  background-color: #f6f6f6;
  margin: 0 auto;
`;

export default class App extends Component {
  state = {
    listaCitas: [],
  };

  agregarCita = (cita) => {
    console.log("la info de la cita es:", cita);
    this.setState({ listaCitas: [...this.state.listaCitas, cita] });
  };
  eliminarCita = (id) => {
    console.log("el id de la cita es:", id);
    const listaActualizada = this.state.listaCitas.filter(
      (cita) => cita.id !== id
    );
    this.setState({ listaCitas: listaActualizada });
  };

  render() {
    return (
      <div>
        <GlobalStyle />
        <Container>
          <Formulario agregarCita={this.agregarCita} />
          <ListaCitas
            listaCitas={this.state.listaCitas}
            eliminarCita={this.eliminarCita}
          />
        </Container>
      </div>
    );
  }
}
