import React from "react";
import { Button, CitaItem } from "./style";

const Cita = ({ info, eliminarCitaLocal, className }) => {
  const { nombre, apellido, direccion, fecha, hora, sintomas } = info;
  return (
    <section className={className}>      
        <CitaItem>
          <strong>Nombre:</strong> {nombre}
        </CitaItem>
        <CitaItem>
          <strong>Apellido:</strong> {apellido}
        </CitaItem>
        <CitaItem>
          <strong>Direccion:</strong> {direccion}
        </CitaItem>
        <CitaItem>
          <strong>Fecha:</strong> {fecha}
        </CitaItem>
        <CitaItem>
          <strong>Hora:</strong> {hora}
        </CitaItem>
        <CitaItem>
          <strong>Sintomas:</strong> {sintomas}
        </CitaItem>
        <Button onClick={eliminarCitaLocal}>Eliminar</Button>      
    </section>
  );
};

export default Cita;
