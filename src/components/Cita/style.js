import styled, { css } from "styled-components";

export const Button = styled.button`
  display: block;
  background-color: #f7797d;
  color: #fff;
  font-size: 0.9rem;
  border: 0px;
  border-radius: 5px;  
  padding: 10px;
  cursor: pointer;
  box-sizing: border-box;
  align-self:flex-end;
  &:hover{
    background-color:#da515d;
  }
`;

export const CitaItem = styled.p`
  display: block;    
  padding: 10px 0;  
  box-sizing: border-box;  
  display:flex;
  justify-content:space-between;
`;
