import React from "react";
import styled from "styled-components";
import Cita from "../Cita";
import Title from "../Title";

export const CitaStyled = styled(Cita)`  
padding: 10px 15px;
box-sizing: border-box;
border: 1px solid #c5c5c5;
background: #ebebeb;
border-radius: 8px;
display:flex;
flex-direction:column;
flex: 0 1 calc(calc(100%/4) - 15px);
`;
const ListaCitas = ({ listaCitas, eliminarCita }) => {
  return (
    <div>
      <Title titulo={"Lista de citas medicas"} />
      <section style={{ display: "flex", marginTop:20, flexWrap:'wrap', gap:15 }}> 
        {listaCitas.length > 0 ? (
          listaCitas.map((cita) => (
            <CitaStyled
              info={cita}
              key={cita.id}
              eliminarCitaLocal={() => eliminarCita(cita.id)}
            />
          ))
        ) : (
          <p>No tienes citas agendadas</p>
        )}
      </section>
    </div>
  );
};

export default ListaCitas;
