import React from "react";
import styled from "styled-components";

const Titulo = styled.h2`
  color: "#7271fb";
  text-align: center;
  margin: 0;
`;

const Title = ({ titulo }) => {
  return <Titulo>{titulo}</Titulo>;
};

export default Title;
