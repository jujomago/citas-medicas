import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";
import { Form, FormGroup, Input, TextArea, Button } from "./style";
import Title from "../Title";

const initialState = {
  cita: {
    nombre: "",
    apellido: "",
    direccion: "",
    fecha: "",
    hora: "",
    sintomas: "",
  },
};

export default class Formulario extends Component {
  constructor() {
    super();
    this.state = {
      ...initialState,
    };
  }

  handleChange = (e) => {
    //console.log(e.target.name)
    // this.setState({ ...this.state.cita, [e.target.name]: e.target.value });
    this.setState({
      cita: { ...this.state.cita, [e.target.name]: e.target.value },
    });
  };

  cualquierNombre = (e) => {
    e.preventDefault();
    console.log("los datos a enviar al padre APP son:", this.state.cita);
  };

  validaDatos = () => {
    const { cita } = this.state;
    let valorRetorno = true;

    if (
      cita.nombre === "" ||
      cita.apellido === "" ||
      cita.direccion === "" ||
      cita.fecha === "" ||
      cita.hora === "" ||
      cita.sintomas === ""
    ) {
      valorRetorno = false;
    }
    return valorRetorno;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.validaDatos()) {
      const nuevaCita = { ...this.state.cita };
      nuevaCita.id = uuidv4();
      this.props.agregarCita(nuevaCita);
      this.setState({ ...initialState });
    } else {
      console.log("todos los campos son requeridos");
    }
  };

  render() {
    return (
      <div>
        <Title titulo="Formulario de Citas medicas" />
        <Form onSubmit={this.handleSubmit}>
          <FormGroup col="2">
            <label>Nombre</label>
            <Input
              type="text"
              name="nombre"
              placeholder="Ingresar nombre"
              onChange={this.handleChange}
              value={this.state.cita.nombre}
            />
          </FormGroup>
          <FormGroup col="2">
            <label>Apellido</label>
            <Input
              type="text"
              name="apellido"
              placeholder="Ingresar apellido"
              onChange={this.handleChange}
              value={this.state.cita.apellido}
            />
          </FormGroup>
          <FormGroup col="1">
            <label>Dirección</label>
            <Input
              type="text"
              name="direccion"
              placeholder="Ingresar dirección"
              onChange={this.handleChange}
              value={this.state.cita.direccion}
            />
          </FormGroup>
          <FormGroup col="3">
            <label>Fecha de cita</label>
            <Input
              type="date"
              name="fecha"
              onChange={this.handleChange}
              value={this.state.cita.fecha}
            />
          </FormGroup>
          <FormGroup col="3">
            <label>Hora</label>
            <Input
              type="time"
              name="hora"
              onChange={this.handleChange}
              value={this.state.cita.hora}
            />
          </FormGroup>
          <FormGroup col="3">
            <label>Descripción</label>
            <TextArea
              type="text"
              name="sintomas"
              onChange={this.handleChange}
              value={this.state.cita.sintomas}
            />
          </FormGroup>
          <FormGroup col="1">
            <Button type="submit">Reservar</Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
